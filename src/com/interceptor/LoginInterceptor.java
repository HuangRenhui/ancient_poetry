package com.interceptor;
/**
* @author 黄仁辉 E-mail:249968839@qq.com
*/
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.HandlerInterceptor;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

public class LoginInterceptor implements HandlerInterceptor {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		Object user = request.getSession().getAttribute("admin");
		if (user == null) {
			System.out.println("尚未登录，调到登录页面");
			//重定向到项目默认界面
			response.sendRedirect("http://localhost:8080/Ancient_poetry/");
			return false;
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}
}
