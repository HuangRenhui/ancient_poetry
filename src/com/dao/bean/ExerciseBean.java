package com.dao.bean;
/**
* @author 黄仁辉 E-mail:249968839@qq.com
*/
public class ExerciseBean {
    private Integer exerciseid;

    private Integer epoetryid;

    private String question;

    private String optiona;

    private String optionb;

    private String optionc;

    private String optiond;

    private String answer;

    private String analysis;
    
    private PoetryBean poetryBean;
    

    public PoetryBean getPoetryBean() {
		return poetryBean;
	}

	public void setPoetryBean(PoetryBean poetryBean) {
		this.poetryBean = poetryBean;
	}

	public Integer getExerciseid() {
        return exerciseid;
    }

    public void setExerciseid(Integer exerciseid) {
        this.exerciseid = exerciseid;
    }

    public Integer getEpoetryid() {
        return epoetryid;
    }

    public void setEpoetryid(Integer epoetryid) {
        this.epoetryid = epoetryid;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question == null ? null : question.trim();
    }

    public String getOptiona() {
        return optiona;
    }

    public void setOptiona(String optiona) {
        this.optiona = optiona == null ? null : optiona.trim();
    }

    public String getOptionb() {
        return optionb;
    }

    public void setOptionb(String optionb) {
        this.optionb = optionb == null ? null : optionb.trim();
    }

    public String getOptionc() {
        return optionc;
    }

    public void setOptionc(String optionc) {
        this.optionc = optionc == null ? null : optionc.trim();
    }

    public String getOptiond() {
        return optiond;
    }

    public void setOptiond(String optiond) {
        this.optiond = optiond == null ? null : optiond.trim();
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer == null ? null : answer.trim();
    }

    public String getAnalysis() {
        return analysis;
    }

    public void setAnalysis(String analysis) {
        this.analysis = analysis == null ? null : analysis.trim();
    }

	
    
}