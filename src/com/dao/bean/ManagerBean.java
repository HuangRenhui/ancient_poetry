package com.dao.bean;
/**
* @author 黄仁辉 E-mail:249968839@qq.com
*/
public class ManagerBean {
    private Integer managerid;

    private String name;

    private String password;

    public Integer getManagerid() {
        return managerid;
    }

    public void setManagerid(Integer managerid) {
        this.managerid = managerid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }


}