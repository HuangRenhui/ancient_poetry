package com.dao.bean;

import java.util.Date;

public class PoetryBean {
	private Integer poetryid;

	private String title;

	private String dynasty;

	private String author;

	private String content;

	private String background;

	private String annotation;

	private String translation;

	private String appreciation;

	private Date date;

	private String image;

	private String judge;

	private Integer flag;
	
	private Integer readingnum;

	public Integer getPoetryid() {
		return poetryid;
	}

	public void setPoetryid(Integer poetryid) {
		this.poetryid = poetryid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title == null ? null : title.trim();
	}

	public String getDynasty() {
		return dynasty;
	}

	public void setDynasty(String dynasty) {
		this.dynasty = dynasty == null ? null : dynasty.trim();
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author == null ? null : author.trim();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content == null ? null : content.trim();
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background == null ? null : background.trim();
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation == null ? null : annotation.trim();
	}

	public String getTranslation() {
		return translation;
	}

	public void setTranslation(String translation) {
		this.translation = translation == null ? null : translation.trim();
	}

	public String getAppreciation() {
		return appreciation;
	}

	public void setAppreciation(String appreciation) {
		this.appreciation = appreciation == null ? null : appreciation.trim();
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image == null ? null : image.trim();
	}

	public String getJudge() {
		return judge;
	}

	public void setJudge(String judge) {
		this.judge = judge == null ? null : judge.trim();
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Integer getReadingnum() {
		return readingnum;
	}

	public void setReadingnum(Integer readingnum) {
		this.readingnum = readingnum;
	}

}