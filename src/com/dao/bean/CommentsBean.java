package com.dao.bean;

import java.util.Date;

public class CommentsBean {
	private Integer commentsid;

	private Integer cuserid;

	private Integer cpoetryid;

	private String content;

	private Integer praise;

	private Integer flag;
	
	private Date commenttime;
	
	private UserBean userBean;

	private PoetryBean poetryBean;

	public Integer getCommentsid() {
		return commentsid;
	}

	public void setCommentsid(Integer commentsid) {
		this.commentsid = commentsid;
	}

	public Integer getCuserid() {
		return cuserid;
	}

	public void setCuserid(Integer cuserid) {
		this.cuserid = cuserid;
	}

	public Integer getCpoetryid() {
		return cpoetryid;
	}

	public void setCpoetryid(Integer cpoetryid) {
		this.cpoetryid = cpoetryid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content == null ? null : content.trim();
	}

	public Integer getPraise() {
		return praise;
	}

	public void setPraise(Integer praise) {
		this.praise = praise;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Date getCommenttime() {
		return commenttime;
	}

	public void setCommenttime(Date commenttime) {
		this.commenttime = commenttime;
	}

	public UserBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}

	public PoetryBean getPoetryBean() {
		return poetryBean;
	}

	public void setPoetryBean(PoetryBean poetryBean) {
		this.poetryBean = poetryBean;
	}

    
}