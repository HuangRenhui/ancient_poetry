package com.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
* @author 黄仁辉 E-mail:249968839@qq.com
*/
import com.dao.bean.ExerciseBean;

public interface ExerciseBeanMapper {
	int deleteByPrimaryKey(Integer exerciseid);

	int insert(ExerciseBean record);

	int insertSelective(ExerciseBean record);

	ExerciseBean selectByPrimaryKey(Integer exerciseid);

	int updateByPrimaryKeySelective(ExerciseBean record);

	int updateByPrimaryKey(ExerciseBean record);

	// 查询所有习题信息
	List<ExerciseBean> selectAll();

	// 根据问题查询习题信息
	List<ExerciseBean> queryBeanByQuestion(@Param(value="question")String question);
	// 根据epoetryid查询对应的所有习题信息
	List<ExerciseBean> queryBeanByEpoetryid(Integer epoetryid);
}