package com.dao.mapper;
/**
* @author 黄仁辉 E-mail:249968839@qq.com
*/
import com.dao.bean.UserBean;

public interface UserBeanMapper {
    int deleteByPrimaryKey(Integer userid);

    int insert(UserBean record);

    int insertSelective(UserBean record);

    UserBean selectByPrimaryKey(Integer userid);

    int updateByPrimaryKeySelective(UserBean record);

    int updateByPrimaryKey(UserBean record);
}