package com.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.dao.bean.CommentsBean;

public interface CommentsBeanMapper {
	int deleteByPrimaryKey(Integer commentsid);

	int insert(CommentsBean record);

	int insertSelective(CommentsBean record);

	CommentsBean selectByPrimaryKey(Integer commentsid);

	int updateByPrimaryKeySelective(CommentsBean record);

	int updateByPrimaryKey(CommentsBean record);

	// 查询所有留言信息
	List<CommentsBean> selectAll();

	// 根据用户名查询留言
	List<CommentsBean> selectBeanByUserName(@Param(value = "WeChatName") String userName);

	// 根据古诗文题目查询留言
	List<CommentsBean> selectBeanByTile(@Param(value = "title") String title);

	// 根据古诗文id根据发布flag属性值
	int updateFlagById(@Param(value = "commentsid")Integer commentsid,@Param(value = "flag")Integer flag);
	//根据古诗文题目查询已发布的留言
	List<CommentsBean> selectBeanByTileFlag(@Param(value = "flag")Integer flag,@Param(value = "title") String title);
}