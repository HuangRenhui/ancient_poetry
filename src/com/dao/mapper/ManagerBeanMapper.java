package com.dao.mapper;
/**
* @author 黄仁辉 E-mail:249968839@qq.com
*/
import com.dao.bean.ManagerBean;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface ManagerBeanMapper {
    int deleteByPrimaryKey(Integer managerid);

    int insert(ManagerBean record);

    int insertSelective(ManagerBean record);

    ManagerBean selectByPrimaryKey(Integer managerid);

    int updateByPrimaryKeySelective(ManagerBean record);

    int updateByPrimaryKey(ManagerBean record);
    //用于登陆时获取的用户名和密码查询
    ManagerBean queryByBean(ManagerBean managerBean);
    //查询所有管理员信息
    List<ManagerBean> queryAll();
    //根据用户名进行模糊查询
	List<ManagerBean> selectBeanByName(@Param(value="name")String name);
}