package com.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
* @author 黄仁辉 E-mail:249968839@qq.com
*/
import com.dao.bean.PoetryBean;

public interface PoetryBeanMapper {
	int deleteByPrimaryKey(Integer poetryid);

	int insert(PoetryBean record);

	int insertSelective(PoetryBean record);

	PoetryBean selectByPrimaryKey(Integer poetryid);

	int updateByPrimaryKeySelective(PoetryBean record);

	int updateByPrimaryKey(PoetryBean record);

	// 查询所有的古诗文id和题目
	List<PoetryBean> queryAll();

	// 进行发布更新发布标志
	int updateFlag(@Param(value = "poetryid") Integer poetryid, @Param(value = "flag") Integer flag);

	// 根据title进行查询诗文信息
	List<PoetryBean> queryByTitle(@Param(value = "title") String title);

	// 根据作者查询诗文信息
	List<PoetryBean> queryByAuthor(@Param(value = "author") String author);

	// 根据朝代查询诗文信息
	List<PoetryBean> queryByDynasty(@Param(value = "dynasty") String dynasty);

	// 查询所有可以发布的诗文信息
	List<PoetryBean> queryAllFlag(@Param(value = "flag") Integer flag);
	
	//根据题目查询所有已发布古诗文
	List<PoetryBean> queryByTitleFlag(@Param(value = "flag") Integer flag, @Param(value = "title") String title);
	
	//根据作者查询所有已发布古诗文
	List<PoetryBean> queryByAuthorFlag(@Param(value = "flag") Integer flag, @Param(value = "author") String author);
	
	// 根据朝代查询所有已发布古诗文
	List<PoetryBean> queryByDynastyFlag(@Param(value = "flag") Integer flag, @Param(value = "dynasty") String dynasty);
	
	// 根据id查询已发布的古诗文信息
	PoetryBean queryBeanById(@Param(value = "flag") Integer flag, @Param(value = "poetryid") Integer poetryid);

	// 更新古诗文的阅读量
	int updateBeanReadingNum(@Param(value = "flag") Integer flag, @Param(value = "readingnum")Integer readingnum, @Param(value = "poetryid") Integer poetryid);
}