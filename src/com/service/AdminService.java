package com.service;
/**
* @author 黄仁辉 E-mail:249968839@qq.com
*/
import com.dao.bean.ManagerBean;
import com.dao.mapper.ManagerBeanMapper;
import com.dao.mapper.PoetryBeanMapper;
import org.junit.Test;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AdminService {
    @Resource
    private PoetryBeanMapper poetryBeanMapper;
    @Resource
    private ManagerBeanMapper managerBeanMapper;

    //登陆操作查询用户名和密码是否存在
    public ManagerBean queryAdmin(ManagerBean managerBean) {

        return managerBeanMapper.queryByBean(managerBean);
    }
    //修改密码
    public  int updateById(ManagerBean managerBean){
        return managerBeanMapper.updateByPrimaryKeySelective(managerBean);
    }
    //查询所有管理员信息
    public List<ManagerBean> queryAll(){
        return managerBeanMapper.queryAll();
    }
    //根据id删除管理员信息
    public int deleteBeanById(Integer managerid){
        return  managerBeanMapper.deleteByPrimaryKey(managerid);
    }
    //根据id查询管理员信息
    public  ManagerBean selectBeanById(Integer id){
        return managerBeanMapper.selectByPrimaryKey(id);
    }
	public int addManage(ManagerBean managerBean) {
		return managerBeanMapper.insert(managerBean);
	}
	//根据用户名模糊查询
	public List<ManagerBean> selectBeanByName(String name) {
		return managerBeanMapper.selectBeanByName(name);
	}
}
