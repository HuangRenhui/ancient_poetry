package com.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.dao.bean.PoetryBean;
import com.dao.mapper.PoetryBeanMapper;

/**
 * @author 黄仁辉 E-mail:249968839@qq.com
 * @version 创建时间：2018年6月11日 下午4:34:49
 */
@Service
public class PoetryService {
	@Resource
	private PoetryBeanMapper poetryBeanMapper;

	// 查询所有的古诗文id和题目
	public List<PoetryBean> queryAll() {
		return poetryBeanMapper.queryAll();
	}
    //根据id删除古诗文
	public int deleteBeanById(int id) {
		return poetryBeanMapper.deleteByPrimaryKey(id);
	}
	//发布诗文更新发布标志
	public int updateFlag(int id, Integer flag) {
		return poetryBeanMapper.updateFlag(id,flag);
	}
	//根据id查询古诗文
	public PoetryBean queryBeanById(Integer id) {
		return poetryBeanMapper.selectByPrimaryKey(id);
	}
	//更新古诗文信息
	public int updateBean(PoetryBean poetryBean) {
		return poetryBeanMapper.updateByPrimaryKeySelective(poetryBean);
	}
	//添加古诗文信息
	public int addBean(PoetryBean poetryBean) {
		return poetryBeanMapper.insertSelective(poetryBean);
	}
	//根据title查询诗文信息
	public List<PoetryBean> queryByTitle(String title) {
		return poetryBeanMapper.queryByTitle(title);
	}
	//根据作者查询诗文信息
	public List<PoetryBean> queryByAuthor(String author) {
		return poetryBeanMapper.queryByAuthor(author);
	}
	//根据朝代查询诗文信息
	public List<PoetryBean> queryByDynasty(String dynasty) {
		return poetryBeanMapper.queryByDynasty(dynasty);
	}

}
