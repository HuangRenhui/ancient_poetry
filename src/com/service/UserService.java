package com.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.dao.bean.CommentsBean;
import com.dao.bean.ExerciseBean;
import com.dao.bean.PoetryBean;
import com.dao.mapper.CommentsBeanMapper;
import com.dao.mapper.ExerciseBeanMapper;
import com.dao.mapper.PoetryBeanMapper;
/**
* @author 黄仁辉 E-mail:249968839@qq.com
*/
import com.dao.mapper.UserBeanMapper;

@Service
public class UserService {
	@Resource
	private UserBeanMapper userBeanMapper;
	@Resource
	private PoetryBeanMapper poetryBeanMapper;
	@Resource
	private CommentsBeanMapper commentsBeanMapper;
	
	
	
	@Resource
	private ExerciseBeanMapper exerciseBeanMapper;

	// 查询所有已发布古诗文
	public List<PoetryBean> queryAllFlag() {
		int flag = 1;
		return poetryBeanMapper.queryAllFlag(flag);
	}

	// 根据朝代查询所有已发布古诗文
	public List<PoetryBean> queryAllDynasty(String dynasty) {
		int flag = 1;
		return poetryBeanMapper.queryByDynastyFlag(flag, dynasty);
	}

	// 根据id查询已发布的古诗文信息
	public PoetryBean queryBeanById(int id) {
		int flag = 1;
		return poetryBeanMapper.queryBeanById(flag, id);
	}

	// 更新古诗文的阅读量
	public int updateBeanReadingNum(Integer readingnum, int id) {
		int flag = 1;
		return poetryBeanMapper.updateBeanReadingNum(flag,readingnum,id);
	}
	//查询古诗文的留言信息
	public List<CommentsBean> queryCommentsFlag(String title) {
		int flag = 1;
		return commentsBeanMapper.selectBeanByTileFlag(flag, title);
	}
	//添加留言信息
	public int addComment(CommentsBean bean) {
		return commentsBeanMapper.insertSelective(bean);
	}
	
	
	
	//根据题目查询所有已发布古诗文
	public List<PoetryBean> queryByTitleFlag(int flag,String title){
		return poetryBeanMapper.queryByTitleFlag(flag,title);
	}
	
	
	//根据作者查询所有已发布古诗文
	public List<PoetryBean> queryByAuthorFlag(int flag,String author){
		return poetryBeanMapper.queryByAuthorFlag(flag,author);
	}
	
	// 根据朝代查询所有已发布古诗文
	public List<PoetryBean> queryByDynastyFlag(int flag,String dynasty){
		return poetryBeanMapper.queryByDynastyFlag(flag,dynasty);
	}
	
	// 根据poetryid查询诗文信息
	public PoetryBean selectByPrimaryKey(Integer poetryid) {
		return poetryBeanMapper.selectByPrimaryKey(poetryid);
	}
	
	// 根据epoetryid查询对应的所有习题信息
	public List<ExerciseBean> queryBeanByEpoetryid(Integer epoetryid) {
		return exerciseBeanMapper.queryBeanByEpoetryid(epoetryid);
	}
	
	// 根据exerciseid查询对应的所有习题信息
	public ExerciseBean selectByExerciseid(Integer exerciseid) {
		return exerciseBeanMapper.selectByPrimaryKey(exerciseid);
	}
	
	// 查询所有习题信息
	public List<ExerciseBean> selectAll(){
		return exerciseBeanMapper.selectAll();
	}
	
	// 查询所有的古诗文id和题目
	public List<PoetryBean> queryAll(){
		return poetryBeanMapper.queryAll();
	}
	//点赞数加1
	public int addPraise(CommentsBean bean) {
		return commentsBeanMapper.updateByPrimaryKeySelective(bean);
	}
}
