package com.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.dao.bean.CommentsBean;
import com.dao.mapper.CommentsBeanMapper;

/**
 * @author 黄仁辉 E-mail:249968839@qq.com
 * @version 创建时间：2018年6月10日 下午9:28:02
 */
@Service
public class CommentService {
	@Resource
	private CommentsBeanMapper commentsBeanMapper;

	// 查询所有留言信息
	public List<CommentsBean> queryAllComments() {
		return commentsBeanMapper.selectAll();
	}

	// 根据id查询留言信息
	public CommentsBean selectBeanById(Integer commentsid) {
		return commentsBeanMapper.selectByPrimaryKey(commentsid);
	}

	// 根据id删除留言
	public int deleteComment(Integer commentsid) {
		return commentsBeanMapper.deleteByPrimaryKey(commentsid);
	}

	// 根据用户名查询留言
	public List<CommentsBean> selectBeanByUserName(String userName) {
		return commentsBeanMapper.selectBeanByUserName(userName);
	}

	// 根据古诗文题目查询留言
	public List<CommentsBean> selectBeanByTile(String title) {
		return commentsBeanMapper.selectBeanByTile(title);
	}
    //根据古诗文id根据发布flag属性值
	public int updateFlagById(Integer commentsId, Integer flag) {
		return commentsBeanMapper.updateFlagById(commentsId,flag);
	}

}
