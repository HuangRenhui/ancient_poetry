package com.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.dao.bean.ExerciseBean;
import com.dao.mapper.ExerciseBeanMapper;

/**
 * @author 黄仁辉 E-mail:249968839@qq.com
 * @version 创建时间：2018年6月11日 上午10:12:26
 */
@Service
public class ExerciseService {
	@Resource
	private ExerciseBeanMapper exerciseBeanMapper;

	// 查询所有习题信息
	public List<ExerciseBean> queryAll() {
		return exerciseBeanMapper.selectAll();
	}

	// 根据id查询习题信息
	public ExerciseBean queryBeanById(Integer exerciseid) {
		return exerciseBeanMapper.selectByPrimaryKey(exerciseid);
	}

	// 根据id删除习题
	public int deleteById(Integer exerciseid) {
		return exerciseBeanMapper.deleteByPrimaryKey(exerciseid);
	}

	// 更新习题信息
	public int updateBean(ExerciseBean exerciseBean) {
		return exerciseBeanMapper.updateByPrimaryKeySelective(exerciseBean);
	}

	// 根据问题查询习题信息
	public List<ExerciseBean> queryBeanByQuestion(String question) {
		return exerciseBeanMapper.queryBeanByQuestion(question);
	}

	// 添加习题信息
	public int insertBean(ExerciseBean exerciseBean) {
		return exerciseBeanMapper.insertSelective(exerciseBean);
	}

}
