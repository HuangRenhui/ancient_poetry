package com.controller;

/**
* @author 黄仁辉 E-mail:249968839@qq.com
*/
import com.dao.bean.ManagerBean;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.service.AdminService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;

@Controller
public class AdminController {
	@Resource
	private AdminService adminService;

	// 登陆操作
	@RequestMapping(value = "/toLogin.do", method = RequestMethod.POST)
	public String toLogin(HttpServletRequest request, HttpSession session) {
		String managerName = request.getParameter("managerName");
		String password = request.getParameter("password");
		if (managerName == null || managerName.trim().equals("") || password == null) {
			String errormessage = "请输入完整的账号或密码";
			request.setAttribute("error", errormessage);
			return "bgsystem/admin/login";
		}
		if (managerName != null && !managerName.trim().equals("") && password != null) {
			ManagerBean managerBean = new ManagerBean();
			managerBean.setName(managerName);
			managerBean.setPassword(password);
			// 登陆查询
			managerBean = adminService.queryAdmin(managerBean);
			if (managerBean == null) {
				// 登陆不成功返回登陆界面
				String errormessage = "账号或密码错误";
				request.setAttribute("error", errormessage);
				return "bgsystem/admin/login";
			} else {
				// 登陆成功返回管理系统主页
				session.setAttribute("admin", managerBean);
				return "bgsystem/admin/index";
			}
		} else {
			return "bgsystem/admin/login";
		}
	}

	// 跳转设置界面修改密码
	@RequestMapping(value = "/manage_toSetting.do")
	public String toSetting(int id, Model model) {
		model.addAttribute("managerid", id);
		return "bgsystem/admin/adminSetting";
	}

	// 修改密码
	@RequestMapping("/manage_toChangePwd.do")
	public String toChangePwd(HttpServletRequest request) {
		String id = request.getParameter("managerId");
		Integer managerId = Integer.parseInt(id);
		String password = request.getParameter("password");
		ManagerBean managerBean = new ManagerBean();
		managerBean.setManagerid(managerId);
		managerBean.setPassword(password);
		adminService.updateById(managerBean);
		return "bgsystem/admin/index";
	}

	// 返回主页
	@RequestMapping("/manage_toAdminIndex.do")
	public String toAdminIndex() {
		return "bgsystem/admin/index";
	}

	// 查看账户信息
	@RequestMapping("/manage_toAccount.do")
	public String toAccount(int id, Model model) {
		ManagerBean managerBean = adminService.selectBeanById(id);
		model.addAttribute("adminBean", managerBean);
		return "bgsystem/admin/adminAccount";
	}

	// 退出系统
	@RequestMapping("/manage_toLogout.do")
	public String toLogout(HttpSession session) {
		session.invalidate();
		return "bgsystem/admin/login";
	}

	// 跳转到管理员信息管理界面
	//pageNo第几页数（默认第一页),pageSize每页显示的数量（默认显示10条数据）
	@RequestMapping("/manage_toManage.do")
	public String toManage(@RequestParam(required=true,defaultValue="1") Integer pageNo,  
            @RequestParam(required=false,defaultValue="8") Integer pageSize,Model model) {
		PageHelper.startPage(pageNo, pageSize);
		List<ManagerBean> listBean = adminService.queryAll();
		PageInfo<ManagerBean> page=new PageInfo<ManagerBean>(listBean);  
		model.addAttribute("listBean", listBean);
		model.addAttribute("page",page);
		return "bgsystem/manager/index";
	}

	// 根据id编辑管理员
	@RequestMapping("/manage_toManageEdit.do")
	public String toManageEdit(int id, Model model) {
		ManagerBean managerBean = adminService.selectBeanById(id);
		model.addAttribute("adminBean", managerBean);
		return "bgsystem/manager/manager_edit";
	}

	// 根据id删除管理员
	@RequestMapping("/manage_toManageDel.do")
	public String toManageDel(int id) {
		adminService.deleteBeanById(id);
		return "redirect:manage_toManage.do";
	}

	// 根据id修改管理员信息
	@RequestMapping("/manage_toManageUpdate.do")
	public String toManageUpdate(HttpServletRequest request) {
		String id = request.getParameter("managerId");
		Integer managerId = Integer.parseInt(id);
		String managerName = request.getParameter("managerName");
		String password = request.getParameter("password");
		ManagerBean managerBean = new ManagerBean();
		managerBean.setManagerid(managerId);
		managerBean.setName(managerName);
		managerBean.setPassword(password);
		adminService.updateById(managerBean);
		return "redirect:manage_toManage.do";
	}

	// 跳转到管理员添加界面
	@RequestMapping("/manage_toManageAddJsp.do")
	public String toManageAddJsp() {
		return "bgsystem/manager/manager_add";
	}

	// 跳转到管理员添加界面
	@RequestMapping("/manage_toManageAdd.do")
	public String toManageAdd(HttpServletRequest request) {
		String managerName = request.getParameter("managerName");
		String password = request.getParameter("password");
		ManagerBean managerBean = new ManagerBean();
		managerBean.setName(managerName);
		managerBean.setPassword(password);
		adminService.addManage(managerBean);
		return "redirect:manage_toManage.do";
	}

	// 根据搜索条件查询管理员信息
	@RequestMapping("/manage_toManageQuery.do")
	public String toManageQueryJsp(@RequestParam(required=true,defaultValue="1") Integer pageNo,  
            @RequestParam(required=false,defaultValue="8") Integer pageSize,HttpServletRequest request, Model model) {
		PageHelper.startPage(pageNo, pageSize);
		String info = request.getParameter("info");
		String search = request.getParameter("search");
		if (search == null || search.trim().equals("")) {
			return "redirect:manage_toManage.do";
		}
		if (info.equals("managerId")) {
			String regex = "[1-9]\\d{0,9}";
			boolean flag = search.matches(regex);
			if (flag) {
				// 输入框的信息是阿拉伯数字，长度为1-10位
				Integer id = Integer.parseInt(search);
				List<ManagerBean> listBean = new ArrayList<ManagerBean>();
				ManagerBean managerBean = adminService.selectBeanById(id);
				if (managerBean == null) {
					PageInfo<ManagerBean> page=new PageInfo<ManagerBean>(listBean);
					model.addAttribute("listBean", managerBean);
					model.addAttribute("page",page);
				} else {
					listBean.add(managerBean);
					PageInfo<ManagerBean> page=new PageInfo<ManagerBean>(listBean);
					model.addAttribute("listBean", listBean);
					model.addAttribute("page",page);
				}
			} else {
				return "redirect:manage_toManage.do";
			}
		} else if (info.equals("managerName")) {
			String name = search.trim();
			List<ManagerBean> listBean = adminService.selectBeanByName(name);
			PageInfo<ManagerBean> page=new PageInfo<ManagerBean>(listBean);
			model.addAttribute("listBean", listBean);
			model.addAttribute("page",page);
		}
		return "bgsystem/manager/manager_query";
	}
}
