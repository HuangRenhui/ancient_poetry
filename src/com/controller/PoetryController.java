package com.controller;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.dao.bean.CommentsBean;
import com.dao.bean.ExerciseBean;
import com.dao.bean.PoetryBean;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.service.CommentService;
import com.service.ExerciseService;
import com.service.PoetryService;

/**
 * @author 黄仁辉 E-mail:249968839@qq.com
 * @version 创建时间：2018年6月10日 下午9:24:30
 */
@Controller
public class PoetryController {
	@Resource
	private CommentService commentService;
	@Resource
	private ExerciseService exerciseService;
	@Resource
	private PoetryService poetryService;

	// 跳转到留言信息index界面
	@RequestMapping("/manage_toComment.do")
	public String toComment(@RequestParam(required=true,defaultValue="1") Integer pageNo,  
            @RequestParam(required=false,defaultValue="8") Integer pageSize,Model model) {
		PageHelper.startPage(pageNo, pageSize);
		List<CommentsBean> listBean = commentService.queryAllComments();
		PageInfo<CommentsBean> page=new PageInfo<CommentsBean>(listBean);  
		model.addAttribute("listBean", listBean);
		model.addAttribute("page",page);
		return "bgsystem/commentsManager/index";
	}

	// 跳转到留言详细信息界面
	@RequestMapping("/manage_toCommentShow.do")
	public String toCommentShow(int id, Model model) {
		CommentsBean bean = commentService.selectBeanById(id);
		model.addAttribute("bean", bean);
		return "bgsystem/commentsManager/comments_show";
	}

	// 点击发布按钮，进行发布
	@RequestMapping("/manage_toCommentUpdateFlag.do")
	public String toCommentUpdateFlag(HttpServletRequest request) {
		String id = request.getParameter("commentsId");
		Integer commentsId = Integer.parseInt(id);
		Integer flag = 1;
		commentService.updateFlagById(commentsId, flag);
		return "redirect:manage_toComment.do";
	}

	// 根据id删除留言，跳转到留言信息index界面
	@RequestMapping("/manage_toCommentDel.do")
	public String toCommentDel(int id) {
		commentService.deleteComment(id);
		return "redirect:manage_toComment.do";
	}

	// 根据搜索条件查询留言信息
	@RequestMapping("/manage_toCommentQuery.do")
	public String toManageQueryJsp(@RequestParam(required=true,defaultValue="1") Integer pageNo,  
            @RequestParam(required=false,defaultValue="8") Integer pageSize,HttpServletRequest request, Model model) {
		PageHelper.startPage(pageNo, pageSize);
		String info = request.getParameter("info");
		String search = request.getParameter("search");
		if (search == null || search.trim().equals("")) {
			return "redirect:manage_toComment.do";
		}
		if (info.equals("commentsId")) {
			String regex = "[1-9]\\d{0,9}";
			boolean flag = search.matches(regex);
			if (flag) {
				// 输入框的信息是阿拉伯数字，长度为1-10位
				Integer id = Integer.parseInt(search);
				List<CommentsBean> listBean = new ArrayList<CommentsBean>();
				CommentsBean commentsBean = commentService.selectBeanById(id);
				if (commentsBean == null) {
					PageInfo<CommentsBean> page=new PageInfo<CommentsBean>(listBean);
					model.addAttribute("listBean", commentsBean);
					model.addAttribute("page",page);
				} else {
					listBean.add(commentsBean);
					PageInfo<CommentsBean> page=new PageInfo<CommentsBean>(listBean);
					model.addAttribute("listBean", listBean);
					model.addAttribute("page",page);
				}
			} else {
				return "redirect:manage_toComment.do";
			}
		} else if (info.equals("wechatname")) {
			String userName = search.trim();
			List<CommentsBean> listBean = commentService.selectBeanByUserName(userName);
			PageInfo<CommentsBean> page=new PageInfo<CommentsBean>(listBean);
			model.addAttribute("listBean", listBean);
			model.addAttribute("page",page);
		} else if (info.equals("title")) {
			String title = search.trim();
			List<CommentsBean> listBean = commentService.selectBeanByTile(title);
			PageInfo<CommentsBean> page=new PageInfo<CommentsBean>(listBean);
			model.addAttribute("listBean", listBean);
			model.addAttribute("page",page);
		}
		return "bgsystem/commentsManager/comments_query";
	}

	// 跳转到习题信息界面
	@RequestMapping("/manage_toExercise.do")
	public String toExercise(@RequestParam(required=true,defaultValue="1") Integer pageNo,  
            @RequestParam(required=false,defaultValue="8") Integer pageSize,Model model) {
		PageHelper.startPage(pageNo, pageSize);
		List<ExerciseBean> listBean = exerciseService.queryAll();
		PageInfo<ExerciseBean> page=new PageInfo<ExerciseBean>(listBean);
		model.addAttribute("listBean", listBean);
		model.addAttribute("page",page);
		return "bgsystem/exercisesManager/index";
	}

	// 根据id进行习题编辑
	@RequestMapping("/manage_toExerciseEdit.do")
	public String toExerciseEdit(int id, Model model) {
		ExerciseBean bean = exerciseService.queryBeanById(id);
		model.addAttribute("bean", bean);
		return "bgsystem/exercisesManager/exercises_edit";
	}

	// 根据id进行习题删除
	@RequestMapping("/manage_toExerciseDel.do")
	public String toExerciseDel(int id) {
		exerciseService.deleteById(id);
		return "redirect:manage_toExercise.do";
	}

	// 根据id进行习题信息更新
	@RequestMapping("/manage_toExerciseUpdate.do")
	public String toExerciseUpdate(HttpServletRequest request) {
		String id = request.getParameter("exerciseId");
		Integer exerciseId = Integer.parseInt(id);
		String question = request.getParameter("question");
		String optionA = request.getParameter("optionA");
		String optionB = request.getParameter("optionB");
		String optionC = request.getParameter("optionC");
		String optionD = request.getParameter("optionD");
		String answer = request.getParameter("answer");
		String analysis = request.getParameter("analysis");
		ExerciseBean exerciseBean = new ExerciseBean();
		exerciseBean.setExerciseid(exerciseId);
		exerciseBean.setQuestion(question);
		exerciseBean.setOptiona(optionA);
		exerciseBean.setOptionb(optionB);
		exerciseBean.setOptionc(optionC);
		exerciseBean.setOptiond(optionD);
		exerciseBean.setAnswer(answer);
		exerciseBean.setAnalysis(analysis);
		exerciseService.updateBean(exerciseBean);
		return "redirect:manage_toExercise.do";
	}

	// 根据搜索条件查询习题信息
	@RequestMapping("/manage_toExerciseQuery.do")
	public String toExerciseQuery(@RequestParam(required=true,defaultValue="1") Integer pageNo,  
            @RequestParam(required=false,defaultValue="8") Integer pageSize,HttpServletRequest request,Model model) {
		PageHelper.startPage(pageNo, pageSize);
		String info = request.getParameter("info");
		String search = request.getParameter("search");
		if (search == null || search.trim().equals("")) {
			return "redirect:manage_toExercise.do";
		}
		if (info.equals("exerciseId")) {
			String regex = "[1-9]\\d{0,9}";
			boolean flag = search.matches(regex);
			if (flag) {
				// 输入框的信息是阿拉伯数字，长度为1-10位
				Integer id = Integer.parseInt(search);
				List<ExerciseBean> listBean = new ArrayList<ExerciseBean>();
				ExerciseBean exerciseBean = exerciseService.queryBeanById(id);
				if (exerciseBean == null) {
					PageInfo<ExerciseBean> page=new PageInfo<ExerciseBean>(listBean);
					model.addAttribute("listBean", exerciseBean);
					model.addAttribute("page",page);
				} else {
					listBean.add(exerciseBean);
					PageInfo<ExerciseBean> page=new PageInfo<ExerciseBean>(listBean);
					model.addAttribute("listBean", listBean);
					model.addAttribute("page",page);
				}
			} else {
				return "redirect:manage_toExercise.do";
			}
		} else if (info.equals("question")) {
			String question = search.trim();
			List<ExerciseBean> listBean = exerciseService.queryBeanByQuestion(question);
			PageInfo<ExerciseBean> page=new PageInfo<ExerciseBean>(listBean);
			model.addAttribute("listBean", listBean);
			model.addAttribute("page",page);
		}
		return "bgsystem/exercisesManager/exercises_query";
	}

	// 跳转到添加习题信息界面
	@RequestMapping("/manage_toExerciseAddJsp.do")
	public String toExerciseAddJsp(Model model) {
		List<PoetryBean> listBean = poetryService.queryAll();
		PageInfo<PoetryBean> page=new PageInfo<PoetryBean>(listBean);
		model.addAttribute("listBean", listBean);
		model.addAttribute("page",page);
		return "bgsystem/exercisesManager/exercises_add";
	}

	// 添加习题信息
	@RequestMapping("/manage_toExerciseAdd.do")
	public String toExerciseAdd(HttpServletRequest request) {
		String question = request.getParameter("question");
		String id = request.getParameter("epoetryId");
		Integer epoetryId = Integer.parseInt(id);
		String optionA = request.getParameter("optionA");
		String optionB = request.getParameter("optionB");
		String optionC = request.getParameter("optionC");
		String optionD = request.getParameter("optionD");
		String answer = request.getParameter("answer");
		String analysis = request.getParameter("analysis");
		ExerciseBean exerciseBean = new ExerciseBean();
		exerciseBean.setQuestion(question);
		exerciseBean.setEpoetryid(epoetryId);
		exerciseBean.setOptiona(optionA);
		exerciseBean.setOptionb(optionB);
		exerciseBean.setOptionc(optionC);
		exerciseBean.setOptiond(optionD);
		exerciseBean.setAnswer(answer);
		exerciseBean.setAnalysis(analysis);
		exerciseService.insertBean(exerciseBean);
		return "redirect:manage_toExercise.do";
	}

	// 跳转到诗文信息管理界面,pageNum查询第几页数据，pageSize每页显示多少条数据
	@RequestMapping("/manage_toPoem.do")
	public String toPoem(@RequestParam(required=true,defaultValue="1") Integer pageNo,  
            @RequestParam(required=false,defaultValue="8") Integer pageSize,Model model) {
		PageHelper.startPage(pageNo, pageSize);
		List<PoetryBean> listBean = poetryService.queryAll();
		PageInfo<PoetryBean> page=new PageInfo<PoetryBean>(listBean);
		model.addAttribute("listBean", listBean);
		model.addAttribute("page",page);
		return "bgsystem/poemManager/index";
	}
	// 根据id编辑诗文信息
	@RequestMapping("/manage_toPoemEditJsp.do")
	public String toPoemEditJsp(int id,int error,Model model){
		PoetryBean bean = poetryService.queryBeanById(id);
		model.addAttribute("bean", bean);
		if(error == 1){
			model.addAttribute("errorImg", "上传文件格式错误,请正确上传文件为图片的格式");
		}
		return "bgsystem/poemManager/poem_edit";
	}
	//根据id进行诗文信息更新
	@RequestMapping("/manage_toPoemUpdate.do")
	public String toPoemUpdate(MultipartFile file,HttpServletRequest request) throws Exception{
		String id = request.getParameter("poetryId");
		Integer poetryId = Integer.parseInt(id);
		String title = request.getParameter("title");
		String author = request.getParameter("author");
		String dynasty = request.getParameter("dynasty");
		String content = request.getParameter("content");
		String background = request.getParameter("background");
		String annotation = request.getParameter("annotation");
		String translation = request.getParameter("translation");
		String appreciation = request.getParameter("appreciation");
		String date = request.getParameter("date");
		
		Date datef = new Date();
		//日期格式化
		DateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
		try {
			datef = format.parse(date);//最后存储数据库的值
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//图片上传
		//保存数据库的路径，即数据库字段image
		String  sqlPath = null;
		//文件本地保存的路径
		String localPath = "f:\\images\\";
		//定义文件名
		String filename = null;
		if(!file.isEmpty()){
			//使用本地时间作为文件名称
			SimpleDateFormat df = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");// 设置日期格式
			String str = df.format(new Date());
			//获得文件类型
			String contentType = file.getContentType();
			//获得文件后缀名
			String suffixName = contentType.substring(contentType.indexOf("/")+1);
			//判断上传文件的格式是否符合图片的格式
			String reg = "(GIF|gif|BMP|bmp|PNG|png|JPEG|jpeg|JPG|jpg)$";
			boolean flag = suffixName.matches(reg);
			if(!flag){
				return "redirect:manage_toPoemEditJsp.do?id="+poetryId+"&error="+1;
			}
			//得到文件名
			filename = str +"."+suffixName;
			//文件保存路径
			file.transferTo(new File(localPath+filename));
		}		
		sqlPath = "/"+filename;
		String judge = request.getParameter("judge");
		PoetryBean poetryBean = new PoetryBean();
		poetryBean.setPoetryid(poetryId);
		poetryBean.setTitle(title);
		poetryBean.setAuthor(author);
		poetryBean.setDynasty(dynasty);
		poetryBean.setContent(content);
		poetryBean.setBackground(background);
		poetryBean.setAnnotation(annotation);
		poetryBean.setTranslation(translation);
		poetryBean.setAppreciation(appreciation);
		poetryBean.setDate(datef);
		poetryBean.setImage(sqlPath);
		poetryBean.setJudge(judge);
		poetryService.updateBean(poetryBean);
		return "redirect:manage_toPoem.do";
	}
	//根据id进行发布，更新flag标志
	@RequestMapping("/manage_toPoemUpdateFlag.do")
	public String toPoemUpdateFlag(int id){
		Integer flag = 1;
		poetryService.updateFlag(id,flag);
		return "redirect:manage_toPoem.do";
	}
	// 根据id删除诗文信息
	@RequestMapping("/manage_toPoemDel.do")
	public String toPoemDel(int id){
		PoetryBean poetryBean = poetryService.queryBeanById(id);
		String image = poetryBean.getImage();
		String path = "f:\\images\\";
		File file = new File(path+image);
		file.delete();
		poetryService.deleteBeanById(id);
		return "redirect:manage_toPoem.do";
	}
	// 跳转到添加诗文信息界面
	@RequestMapping("/manage_toPoemAddJsp.do")
	public String toPoemAddJsp(int id,Model model){
		if(id == 1){
			model.addAttribute("errorImg", "上传文件格式错误,请正确上传文件为图片的格式");
		}
		return "bgsystem/poemManager/poem_add";
	}
	//添加诗文信息
	@RequestMapping("/manage_toPoemAdd.do")
	public String toPoemAdd(MultipartFile file,HttpServletRequest request,Model model) throws Exception{
		String title = request.getParameter("title");
		String author = request.getParameter("author");
		String dynasty = request.getParameter("dynasty");
		String content = request.getParameter("content");
		String background = request.getParameter("background");
		String annotation = request.getParameter("annotation");
		String translation = request.getParameter("translation");
		String appreciation = request.getParameter("appreciation");
		String date = request.getParameter("date");
		Date datef = new Date();
		//日期格式化
		DateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
		try {
			datef = format.parse(date);//最后存储数据库的值
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//图片上传
		//保存数据库的路径，即数据库字段image
		String  sqlPath = null;
		//文件本地保存的路径
		String localPath = "f:\\images\\";
		//定义文件名
		String filename = null;
		if(!file.isEmpty()){
			//使用本地时间作为文件名称
			SimpleDateFormat df = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");// 设置日期格式
			String str = df.format(new Date());
			//获得文件类型
			String contentType = file.getContentType();
			//获得文件后缀名
			String suffixName = contentType.substring(contentType.indexOf("/")+1);
			//判断上传文件的格式是否符合图片的格式
			String reg = "(GIF|gif|BMP|bmp|PNG|png|JPEG|jpeg|JPG|jpg)$";
			boolean flag = suffixName.matches(reg);
			if(!flag){		
				return "redirect:manage_toPoemAddJsp.do?id=1";
			}
			//得到文件名
			filename = str +"."+suffixName;
			//文件保存路径
			file.transferTo(new File(localPath+filename));
		}		
		sqlPath = "/"+filename;
		String judge = request.getParameter("judge");
		PoetryBean poetryBean = new PoetryBean();
		poetryBean.setTitle(title);
		poetryBean.setAuthor(author);
		poetryBean.setDynasty(dynasty);
		poetryBean.setContent(content);
		poetryBean.setBackground(background);
		poetryBean.setAnnotation(annotation);
		poetryBean.setTranslation(translation);
		poetryBean.setAppreciation(appreciation);
		poetryBean.setDate(datef);
		poetryBean.setImage(sqlPath);
		poetryBean.setJudge(judge);
		poetryService.addBean(poetryBean);
		return "redirect:manage_toPoem.do";
	}
	// 根据搜索条件搜索诗文信息manage_toPoemQuery.do
	@RequestMapping("/manage_toPoemQuery.do")
	public String toPoemQuery(@RequestParam(required=true,defaultValue="1") Integer pageNo,  
            @RequestParam(required=false,defaultValue="8") Integer pageSize,HttpServletRequest request, Model model){
		PageHelper.startPage(pageNo, pageSize);
		String info = request.getParameter("info");
		String search = request.getParameter("search");
		if (search == null || search.trim().equals("")) {
			return "redirect:manage_toPoem.do";
		}
		if (info.equals("poetryId")) {
			String regex = "[1-9]\\d{0,9}";
			boolean flag = search.matches(regex);
			if (flag) {
				// 输入框的信息是阿拉伯数字，长度为1-10位
				Integer id = Integer.parseInt(search);
				List<PoetryBean> listBean = new ArrayList<PoetryBean>();
				PoetryBean poetryBean = poetryService.queryBeanById(id);
				if (poetryBean == null) {
					PageInfo<PoetryBean> page=new PageInfo<PoetryBean>(listBean);
					model.addAttribute("listBean", poetryBean);
					model.addAttribute("page",page);
				} else {
					listBean.add(poetryBean);
					PageInfo<PoetryBean> page=new PageInfo<PoetryBean>(listBean);
					model.addAttribute("listBean", listBean);
					model.addAttribute("page",page);
				}
			} else {
				return "redirect:manage_toPoem.do";
			}
		}
		else if (info.equals("title")) {
			String title = search.trim();
			List<PoetryBean> listBean = poetryService.queryByTitle(title);
			PageInfo<PoetryBean> page=new PageInfo<PoetryBean>(listBean);
			model.addAttribute("listBean", listBean);
			model.addAttribute("page",page);
		}
		else if (info.equals("author")) {
			String author = search.trim();
			List<PoetryBean> listBean = poetryService.queryByAuthor(author);
			PageInfo<PoetryBean> page=new PageInfo<PoetryBean>(listBean);
			model.addAttribute("listBean", listBean);
			model.addAttribute("page",page);
		}
		else if (info.equals("dynasty")) {
			String dynasty = search.trim();
			List<PoetryBean> listBean = poetryService.queryByDynasty(dynasty);
			PageInfo<PoetryBean> page=new PageInfo<PoetryBean>(listBean);
			model.addAttribute("listBean", listBean);
			model.addAttribute("page",page);
		}
		return "bgsystem/poemManager/poem_query";
	}
}
