package com.controller;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dao.bean.CommentsBean;
import com.dao.bean.ExerciseBean;
import com.dao.bean.PoetryBean;
/**
* @author 黄仁辉 E-mail:249968839@qq.com
*/
import com.service.UserService;

@Controller
public class UserController {
	
    @Resource
    private UserService userService;
    
    //跳转到用户界面
    @RequestMapping("/wxx.do")
    public String toWX(){
    	return "redirect:user_toIndex.do";
    }
    //跳转到用户界面
    @RequestMapping("/user_toIndex.do")
    public String toIndex(Model model){
    	List<PoetryBean> listBean = userService.queryAllFlag();
    	model.addAttribute("listBean", listBean);
    	return "fgsystem/index";
    }
   //跳转到分类界面
    @RequestMapping("/user_toClassListJsp.do")
    public String toClassListJsp(Model model){
    	List<PoetryBean> listBean = userService.queryAllFlag();
    	model.addAttribute("listBean", listBean);
    	return "fgsystem/classification-list";
    }
    //古诗文分类查询，id=1唐朝  id=2宋朝
    @RequestMapping("/user_toClassQuery.do")
    public String toClassListJsp(int id,Model model){
    	if(id == 1){
    		String dynasty = "唐";
    		List<PoetryBean> listBean = userService.queryAllDynasty(dynasty);
        	model.addAttribute("listBean", listBean);
    	}else if(id == 2){
    		String dynasty = "宋";
    		List<PoetryBean> listBean = userService.queryAllDynasty(dynasty);
        	model.addAttribute("listBean", listBean);
    	}else{   		
    		return "redirect:user_toClassListJsp.do";
    	}
		return "fgsystem/classification-query-list";
    }
    
    //跳转到古诗文详细信息界面
    @RequestMapping("/user_toPoemContentJsp.do")
    public String toPoemContentJsp(int id,int flag,Model model){ 
    	PoetryBean bean = userService.queryBeanById(id);
    	if(flag == 1){
    		Integer readingnum = bean.getReadingnum();
    		readingnum += 1;
    		userService.updateBeanReadingNum(readingnum,id);//更新古诗文的阅读量
    		bean = userService.queryBeanById(id);    		
    	}
    	List<CommentsBean> commentsList = userService.queryCommentsFlag(bean.getTitle());
    	model.addAttribute("bean", bean);
    	model.addAttribute("commentsList", commentsList);
    	return "fgsystem/class-poem-content";
    }
    
    //跳转到阅读界面
    @RequestMapping("/user_toReadingListJsp.do")
    public String toReadingListJsp(Model model){
    	List<PoetryBean> listBean = userService.queryAllFlag();
    	model.addAttribute("listBean", listBean);
    	return "fgsystem/reading-content-list";
    }
    //跳转到编写留言界面
    @RequestMapping("/user_toReviewContent.do")
    public String toReviewContent(int id,Model model){
    	PoetryBean bean = userService.queryBeanById(id);
    	model.addAttribute("bean", bean);
		return "fgsystem/review-content";
    }
    //添加留言信息
    @RequestMapping("/user_toPoemContentAdd.do")
    public String toPoemContentAdd(int id,HttpServletRequest request){
    	String content = request.getParameter("content");
    	Date date = new Date();
    	CommentsBean bean = new CommentsBean();
    	bean.setCpoetryid(id);
    	bean.setContent(content);
    	bean.setCommenttime(date);
    	userService.addComment(bean);
    	return "redirect:user_toPoemContentJsp.do?id="+id+"&flag=0";
    }
    //点赞数添加
    @RequestMapping("/user_toAddPraise.do")
    public String toAddPraise(int praise,int id){
    	praise += 1;
    	CommentsBean bean = new CommentsBean();
    	bean.setCommentsid(id);
    	bean.setPraise(praise);
    	userService.addPraise(bean);
    	return "redirect:user_toPoemContentJsp.do?id="+id+"&flag=0";
    }
       // 根据搜索条件搜索诗文信息
 	@RequestMapping("/user_toPoemQuery.do")
    public String user_toPoemQuery(HttpServletRequest request, Model model) {
 		int flag = 1;
 		String search = request.getParameter("search");
 		List<PoetryBean> listBean = null;
 		if(!userService.queryByTitleFlag(flag,search).isEmpty()){
 			listBean = userService.queryByTitleFlag(flag,search);
 		}else if(!userService.queryByAuthorFlag(flag,search).isEmpty()) {
 			listBean = userService.queryByAuthorFlag(flag,search);
 		}else if(!userService.queryByDynastyFlag(flag,search).isEmpty()){
 			listBean = userService.queryByDynastyFlag(flag,search);
 		}
 		model.addAttribute("listBean", listBean);
 		return "fgsystem/poem-search";
 	}
    // 根据poetryid找到诗文信息并跳到具体页面
 	@RequestMapping("/user_toPoemContentByPoetryid.do")
 	public String user_toPoemContentJsp(int id, Model model) {
 		PoetryBean bean = userService.queryBeanById(id);
 		List<CommentsBean> commentsList = userService.queryCommentsFlag(bean.getTitle());
    	model.addAttribute("bean", bean);
    	model.addAttribute("commentsList", commentsList);
 		return "redirect:user_toPoemContentJsp.do?id="+id+"&flag=0";
 	}
 	
 	// 根据epoetryid查询对应的所有习题信息
 	@RequestMapping("/user_toExerciseContentListByPoetryid.do")
 	public String user_toExerciseContentListByPoetryid(int id, Model model) {
 		List<ExerciseBean> listBean = userService.queryBeanByEpoetryid(id);
 		model.addAttribute("listBean", listBean);
		return "fgsystem/exercise-content-list";
 	}
 	
    // 根据exerciseid查询对应的习题信息
 	@RequestMapping("/user_toExerciseContentByExerciseid.do")
 	public String user_toExerciseContentByExerciseid(int id, Model model) {
 		ExerciseBean bean = userService.selectByExerciseid(id);
 		model.addAttribute("bean", bean);
 		return "fgsystem/exercise-content";
 	}
 	
    // 根据exerciseid查询对应的习题答案解析信息
 	@RequestMapping("/user_toExerciseAnswerByExerciseid.do")
 	public String user_toExerciseAnswerByExerciseid(int id, HttpServletRequest request,Model model) {
 		ExerciseBean bean = userService.selectByExerciseid(id);
 		String answer = request.getParameter("ans");
 		int flag = 0;
 		if(bean.getAnswer().equals(answer)) {
 			flag = 1;
 		}
 		model.addAttribute("bean", bean);
 		model.addAttribute("flag", flag);
 		model.addAttribute("answer",answer);
 		return "fgsystem/exercise-answer";
 	}
 	
    // 查询所有习题信息
 	@RequestMapping("/user_toExerciseAll.do")
 	public String user_toExerciseAll(Model model) {
 		List<ExerciseBean> listBean = userService.selectAll();
 		model.addAttribute("listBean", listBean);
 		return "fgsystem/exercise-content-list";
 	}
 	
    //跳转到赏析界面
 	@RequestMapping("/user_toAppreciationListJsp.do")
 	public String user_toAppreciationListJsp(Model model) {
 		List<PoetryBean> listBean = userService.queryAllFlag();
 		
 		for(int i=0;i<listBean.size()-1;i++) {
 			String str = listBean.get(i).getAppreciation().substring(0, 11);
 			listBean.get(i).setAppreciation(str);
 		}
 		
 		model.addAttribute("listBean", listBean);
 		return "fgsystem/appreciation-content-list";
 	}
}
