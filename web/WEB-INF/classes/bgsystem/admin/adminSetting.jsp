<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	request.setAttribute("basePath", basePath);
%>

<!DOCTYPE HTML>
<html>
<head>
<base href="<%=basePath%>">
<title>管理员设置个人信息页</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript">
		
	 addEventListener("load", function() { 
		 setTimeout(hideURLbar, 0); 
		 }, false); 
	 function hideURLbar(){ 
		 window.scrollTo(0,1);
		 } 

</script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet">
<!-- //font-awesome icons CSS -->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet'
	type='text/css' />
<!-- side nav css file -->

<!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- //js-->

<!--webfonts-->
<link
	href="http://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext"
	rel="stylesheet">
<!--//webfonts-->

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->

<!--   base css   -->
<link rel="stylesheet" href="css/base.css" />
<!--   //base css   -->
<script type="text/javascript">
//判断密码符合否
function checkpwd() {  
    var check; 
    var password = document.getElementById("password").value;  
    if (password.length < 6 || password.length > 20) {  
        alert("密码输入不合法，请重新输入!密码长度为任意字符6-20位");
        document.getElementById("password").focus();
        check = false;  
    }
    return check;  
}  


//提交表单时所有都验证一遍(若任何一个验证不通过，则返回为false，阻止表单提交)
function check() {  
    var check =  checkpwd();  
    return check;    
}  
</script>
</head>
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!-- 页头  -->
		<jsp:include page="/viewpage/bgsystem/base/header.jsp"></jsp:include>
		<!-- //页头  -->

		<!--左固定导航-->
		<jsp:include page="/viewpage/bgsystem/base/sidebar.jsp"></jsp:include>
		<!--//左固定导航-->
		<!-- 主要内容开始-->
		<div id="page-wrapper">
			<!--    当前系统管理员修改个人登录密码开始            -->
			<div class="setting-page">
				<h2 class="title1">修改登录密码</h2>
				<div class="blank-page widget-shadow scroll" id="style-2 div1"
					style="position: relative;">
					<div class="row">
						<div class="form-three widget-shadow">
							<form class="form-horizontal"
								action="${pageContext.request.contextPath}/manage_toChangePwd.do"
								method="post" onsubmit="return check()">
								<div class="form-group" style="display: none;">
									<label for="managerId" class="col-sm-2 control-label">管理员</label>
									<div class="col-sm-8">
										<input type="text" class="form-control1" id="managerId"
											name="managerId" value="${managerid}">
									</div>
								</div>
								<div class="form-group">
									<label for="password" class="col-sm-2 control-label">密码</label>
									<div class="col-sm-8">
										<input type="text" class="form-control1" id="password"
											name="password" value="" onchange="checkpwd()">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"></label>
									<div class="col-sm-8" style="text-align: center;">
										<!--   修改按钮       -->
										<button type="submit" class="bt">
											<i class="fa fa-edit" aria-hidden="true"></i>修改
										</button>
										<!--   //修改按钮       -->
										<!--   返回按钮       -->
										<a
											href="${pageContext.request.contextPath}/manage_toAdminIndex.do">
											<button type="button" class="bt">
												<i class="fa fa-reply" aria-hidden="true"></i>返回
											</button>
										</a>
										<!--   //返回按钮       -->
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!--    当前系统管理员修改个人登录密码结束            -->
		</div>
		<!-- 主要内容结束-->
		<!--页脚-->
		<jsp:include page="/viewpage/bgsystem/base/footer.jsp"></jsp:include>
		<!--//页脚-->
	</div>

	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
		$('.sidebar-menu').SidebarNav()
	</script>
	<!-- //side nav js -->

	<!-- Classie -->
	<!-- for toggle left push menu script -->
	<script src="js/classie.js"></script>
	<script>
		var menuLeft = document.getElementById('cbp-spmenu-s1'), showLeftPush = document
				.getElementById('showLeftPush'), body = document.body;

		showLeftPush.onclick = function() {
			classie.toggle(this, 'active');
			classie.toggle(body, 'cbp-spmenu-push-toright');
			classie.toggle(menuLeft, 'cbp-spmenu-open');
			disableOther('showLeftPush');
		};

		function disableOther(button) {
			if (button !== 'showLeftPush') {
				classie.toggle(showLeftPush, 'disabled');
			}
		}
	</script>
	<!-- //Classie -->
	<!-- //for toggle left push menu script -->

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js">
		
	</script>
	<!-- Bootstrap Core JavaScript -->

</body>
</html>