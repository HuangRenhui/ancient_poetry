<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <link rel="stylesheet" href="css/amazeui.min.css">
  <link rel="stylesheet" href="css/wap.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <script src="js/jquery-3.3.1.min.js"></script>
  <title>习题答案与解析内容页</title>
</head>
<body style="background:#ececec">
<div class="poem_mian" >
    <!--   头部   -->
    <div class="poem_head">
      <header data-am-widget="header"
              class="am-header am-header-default poem_head_block">
          <div class="am-header-left am-header-nav ">
              <!-- 返回超链接   -->
              <a href="${pageContext.request.contextPath}/user_toExerciseContentByExerciseid.do?id=${bean.exerciseid}" class="iconfont poem_head_jt_ico">&#xe601;</a>
              <!-- 返回超链接   -->
          </div>

          <h1 class="am-header-title poem_article_user">
            <span class="poem_name">每天一篇古诗文</span>
          </h1>
      </header>
    </div>
  <!--   头部   -->

  <!--   主体内容   -->
  <div class="poem_content">
  <div class="poem_content_block">
    <article data-am-widget="paragraph" class="am-paragraph am-paragraph-default poem_content_article" data-am-paragraph="{ tableScrollable: true, pureview: true }">
    <!--      习题题目        -->
    <h1 class="poem_article_title">习题练习</h1>
    <!--      //习题题目        -->
  
    <!--      诗文图片        -->
    
    <!--      //诗文图片        -->

    <p><blockquote><p>题目</p></blockquote></p>
    <!--      习题内容        -->
    <p style="text-align: center;">
      &nbsp;&nbsp;&nbsp;&nbsp;${bean.question}<br>
      A.<input type="radio" name="optionA">${bean.optiona}&nbsp;&nbsp;
      B.<input type="radio" name="optionB">${bean.optionb}&nbsp;&nbsp;
      C.<input type="radio" name="optionC">${bean.optionc}&nbsp;&nbsp;
      D.<input type="radio" name="optionD">${bean.optiond}&nbsp;&nbsp;
    </p>
    <!--      //习题内容        -->
    
    <c:if test="${flag == 1 }">
        <c:out value="恭喜你答对了！"></c:out><br>
    </c:if>
    <c:if test="${flag == 0 }">
        <c:out value="很遗憾，你答错了！"></c:out><br>
    </c:if>
    
    <p><blockquote><p>你的答案：${answer}</p></blockquote></p>
    <p><blockquote><p>正确答案：${bean.answer}</p></blockquote></p>
    <!--      习题答案        -->
    <!-- 
    <p>
      C. 庐山
    
    </p>
     -->
    
    <!--      //习题答案      -->

    <p><blockquote><p>解析</p></blockquote></p>
    <!--      习题解析        -->
    <p>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${bean.analysis}，应选${bean.answer}。
    </p>
    <!--      //习题解析        -->
    </article>
    
    <div style="width:100%;height:50px;margin-top:20px;"></div>
    
  </div>

  <!--      页脚       -->
  <div class="poem_article_footer_info">Copyright(c)2018 软工81班.阿尔法小队</div>
  <!--      页脚       -->

  <!--      js       -->
  <script src="js/jquery.min.js"></script>
  <script src="js/amazeui.min.js"></script>
  <script>
  $(function(){

      // 动态计算古诗文列表文字样式
      auto_resize();
      $(window).resize(function() {
          auto_resize();
      });
      $('.am-list-thumb img').load(function(){
          auto_resize();
      });
      $('.poem_article_like li:last-child').css('border','none');
          function auto_resize(){
          $('.poem_list_one_nr').height($('.poem_list_one_img').height());
                  // alert($('.poem_list_one_nr').height());
      }
          $('.poem_article_user').on('click',function(){
              if($('.poem_article_user_info_tab').hasClass('poem_article_user_info_tab_show')){
                  $('.poem_article_user_info_tab').removeClass('poem_article_user_info_tab_show').addClass('poem_article_user_info_tab_cloes');
              }else{
                  $('.poem_article_user_info_tab').removeClass('poem_article_user_info_tab_cloes').addClass('poem_article_user_info_tab_show');
              }
          });

          $('.poem_head_gd_ico').on('click',function(){
              $('.poem_more_list').addClass('poem_more_list_show');
         });
          $('.poem_more_close').on('click',function(){
              $('.poem_more_list').removeClass('poem_more_list_show');
          });
  });
  </script>
  <!--      //js       -->
</body>
</html>