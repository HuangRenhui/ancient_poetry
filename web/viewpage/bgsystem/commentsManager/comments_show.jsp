<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
<head>
	<title>留言信息管理页</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />	
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<!-- font-awesome icons CSS -->
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<!-- //font-awesome icons CSS -->
	<!-- side nav css file -->
	<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
	<!-- side nav css file -->
	<!-- js-->
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/modernizr.custom.js"></script>
	<!-- //js-->
	<!--webfonts-->
	<link href="http://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
	<!--//webfonts--> 
	<!-- Metis Menu -->
	<script src="js/metisMenu.min.js"></script>
	<script src="js/custom.js"></script>
	<link href="css/custom.css" rel="stylesheet">
	<!--//Metis Menu -->
	<!--   base css   -->
	<link rel="stylesheet" href="css/base.css" />
	<!--   //base css   -->
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
	
	<!-- 页头  -->
	<jsp:include page="/viewpage/bgsystem/base/header.jsp"></jsp:include>
	<!-- //页头  -->

	<!--左固定导航-->
	<jsp:include page="/viewpage/bgsystem/base/sidebar.jsp"></jsp:include>
	<!--//左固定导航-->
    
	<!-- 主要内容开始-->
	<div id="page-wrapper">
				<!--    留言详情内容开始            -->
		<div class="conceal" id="ct3">
			<h2 class="title1">留言信息</h2>
			<div class="blank-page widget-shadow scroll" id="style-2 div1" style="position: relative;">
				<div class="row">
					<div class="form-three widget-shadow">
						<form class="form-horizontal" action="${pageContext.request.contextPath}/manage_toCommentUpdateFlag.do">
							<div class="form-group" style="display:none;">
								<label for="commentsId" class="col-sm-2 control-label">留言号</label>
								<div class="col-sm-8">
									<input type="text" class="form-control1" id="commentsId" name="commentsId" readonly="readonly" value="${bean.commentsid }">
								</div>
							</div>
							<div class="form-group">
								<label for="userName" class="col-sm-2 control-label">用户名</label>
								<div class="col-sm-8">
									<input type="text" class="form-control1" id="userName" name="userName" readonly="readonly" value="${bean.userBean.getWechatname() }">
								</div>
							</div>
							<div class="form-group">
								<label for="title" class="col-sm-2 control-label">古诗文题目</label>
								<div class="col-sm-8">
									<input type="text" class="form-control1" id="title" name="title" readonly="readonly" value="${bean.poetryBean.getTitle() }">
								</div>
							</div>
							<div class="form-group">
								<label for="content" class="col-sm-2 control-label">留言内容</label>
								<div class="col-sm-8"><textarea name="content" id="content" cols="50" rows="5" style="border:1px solid #C8C8C8;" readonly="readonly">${bean.content }</textarea></div>
							</div>
							<div class="form-group">
								<label for="praise" class="col-sm-2 control-label">点赞数</label>
								<div class="col-sm-8">
									<input type="text" class="form-control1" id="praise" name="praise" readonly="readonly" value="${bean.praise }">
								</div>
							</div>
							<div class="form-group">
								<label for="submit" class="col-sm-2 control-label"></label>
								<div class="col-sm-8" style="text-align: center;">
									<!--   发布按钮       -->
									<button type="submit" class="upload"><i class="fa fa-upload" aria-hidden="true"></i>发布</button>
									<!--   //发布按钮       -->
									<!--   删除按钮       -->
									<a href="${pageContext.request.contextPath}/manage_toCommentDel.do?id=${bean.commentsid }" onclick="return confirm('确定删除吗？')"><button type="button" class="del"><i class="fa times图标的示例 fa-times" aria-hidden="true"></i>删除</button></a>
									<!--   //删除按钮       -->
									<!--   返回按钮      -->
									<a href="${pageContext.request.contextPath}/manage_toComment.do"><button type="button" class="ret"><i class="fa fa-reply" aria-hidden="true"></i>返回</button></a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--    留言详情内容结束            -->
	</div>
	<!-- 主要内容结束-->
	<!--页脚-->
	<jsp:include page="/viewpage/bgsystem/base/footer.jsp"></jsp:include>
	<!--//页脚-->
</div>
	
	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
	  $('.sidebar-menu').SidebarNav()
	</script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
	<script src="js/classie.js"></script>
	<script>
		var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
			showLeftPush = document.getElementById( 'showLeftPush' ),
			body = document.body;
			
		showLeftPush.onclick = function() {
			classie.toggle( this, 'active' );
			classie.toggle( body, 'cbp-spmenu-push-toright' );
			classie.toggle( menuLeft, 'cbp-spmenu-open' );
			disableOther( 'showLeftPush' );
		};
		
		function disableOther( button ) {
			if( button !== 'showLeftPush' ) {
				classie.toggle( showLeftPush, 'disabled' );
			}
		}
	</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>
	<!-- Bootstrap Core JavaScript -->
   
</body>
</html>