<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
	  <!--左固定导航-->
	  <aside class="sidebar-left">
	  <!--  导航开始       -->
      <nav class="navbar navbar-inverse">
          <div class="navbar-header">
	            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
	            </button>
	            <h1 style="margin-top:20px;"><span class="navbar-brand">菜单列表</span></h1>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="sidebar-menu">
              <li class="header">常用功能</li>
              <!-- 前台首页  -->
              <li class="treeview">
                <a href="${pageContext.request.contextPath}/user_toIndex.do">
                	<i class="fa fa-home"></i> 
                	<span>前台首页</span>
                </a>
              <!-- 诗文信息管理栏  -->
              <li class="treeview">
                <a>
                	<i class="fa fa-file"></i> 
                	<span>诗文信息管理</span>
                	<i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="${pageContext.request.contextPath}/manage_toPoem.do"><i class="fa fa-angle-right"></i>诗文信息</a></li>
                </ul>
              </li>
              <!-- 习题信息管理栏  -->
			  <li class="treeview">
                <a>
                	<i class="fa fa-file-text"></i>
                		<span>习题信息管理</span>
                	<i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="${pageContext.request.contextPath}/manage_toExercise.do"><i class="fa fa-angle-right"></i>习题信息</a></li>             
                </ul>
             </li>
              <!-- 留言信息管理栏  -->
              <li class="treeview">
                <a>
	                <i class="fa fa-comments"></i>
	                <span>留言信息管理</span>
	                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="${pageContext.request.contextPath}/manage_toComment.do"><i class="fa fa-angle-right"></i>留言信息</a></li>             
                </ul>
              </li>
              <!-- 管理员信息管理栏  -->
              <li class="treeview">
                <a>
	                <i class="fa fa-user"></i>
	                <span>管理员信息管理</span>
	                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="${pageContext.request.contextPath}/manage_toManage.do"><i class="fa fa-angle-right"></i>管理员信息</a></li>             
                </ul>
              </li>
              <li class="header">系统工具</li>
              <li><a href="#"><i class="fa fa-angle-right text-red"></i> <span>访问量统计</span></a></li>
            </ul>
          </div>
      </nav>
      <!-- 导航结束 -->
    </aside>
	</div>
	<!--左固定导航-->