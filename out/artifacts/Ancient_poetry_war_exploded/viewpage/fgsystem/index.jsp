<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="stylesheet" href="css/amazeui.min.css">
<link rel="stylesheet" href="css/wap.css">
<link rel="stylesheet" href="css/font-awesome.css">
<title>用户界面首页</title>
</head>
<body>

  <!--     顶部导航        -->
  <nav class="nav-bar">
      <img src="images/nav-logo.png" />
      <div class="input-text">
      <form action="${pageContext.request.contextPath}/user_toPoemQuery.do" method="post">
      <input class="poem-search" type="text" placeholder="古诗文搜索..." name="search">
      <!--     古诗文搜索按钮        --> 
      <button class="fa fa-search" type="submit"></button>
      <!-- <i class="fa fa-search"></i> -->
      <!--     //古诗文搜索按钮        -->
      </form>
    </div>
  </nav>
  <!--     //顶部导航        -->

  <!--      轮播图开始        -->
  <div class="poem_mian" id="top">
    <div data-am-widget="slider" class="am-slider am-slider-a1" data-am-slider='{"directionNav":false}' >
    <ul class="am-slides">
        <li>
            <img src="images/fl01.png">
            <div class="poem_slider_font">
                <span class="poem_slider_emoji">静夜思</span>
                <span>举头望明月，低头思故乡</span>
            </div>
            <div class="poem_slider_shadow"></div>
        </li>
        <li>
            <img src="images/fl02.png">
            <div class="poem_slider_font">
                <span class="poem_slider_emoji">望岳　</span>
                <span>会当凌绝顶，一览众山小</span>
            </div>
            <div class="poem_slider_shadow"></div>
        </li>
        <li>
              <img src="images/fl03.png">
              <div class="poem_slider_font">
                  <span class="poem_slider_emoji"> 如梦令·昨夜雨疏风骤　</span>
                  <span>知否，知否，应是绿肥红瘦</span>
              </div>
              <div class="poem_slider_shadow"></div>
        </li>
    </ul>
  </div>
  <!--      轮播图结束        -->

  <!--      圆形按钮导航     -->
  <div class="poem_circle_nav">
    <ul class="poem_circle_nav_list">
        <!--阅读导航-->
        <li><a href="${pageContext.request.contextPath}/user_toReadingListJsp.do" class="iconfont poem_nav_yuedu ">&#xe62c;</a><span>阅读</span></li>
        <!--//阅读导航-->
        <!--习题导航-->
        <li><a href="${pageContext.request.contextPath}/user_toExerciseAll.do" class="iconfont poem_nav_xiti ">&#xe629;</a><span>习题</span></li>
        <!--//习题导航-->
        <!--分类导航-->
        <li><a href="${pageContext.request.contextPath}/user_toClassListJsp.do" class="iconfont poem_nav_fenlei ">&#xe602;</a><span>分类</span></li>
        <!--//分类导航-->
        <!--赏析导航-->
        <li><a href="${pageContext.request.contextPath}/user_toAppreciationListJsp.do" class="iconfont poem_nav_shangxi ">&#xe612;</a><span>赏析</span></li>
        <!--赏析导航-->
    </ul>
  </div>
  <!--      //圆形按钮导航        -->

  <!--      主要内容        -->
  <div class="poem_content_main">
    <div data-am-widget="list_poems" class="am-list-poems am-list-poems-default" >
    <div class="am-list-poems-bd">
    <ul class="am-list">
       <!--for循环-->
       <c:forEach items="${listBean}" var="bean" begin="0" end="4" step="1" >
        <li class="am-g am-list-item-desced am-list-item-thumbed am-list-item-thumb-right poem_list_one_block">
          <a href="${pageContext.request.contextPath}/user_toPoemContentJsp.do?id=${bean.poetryid}&flag=1">
            <div class=" am-u-sm-8 am-list-main poem_list_one_nr">
                <h3 class="am-list-item-hd poem_list_one_bt">${bean.title}</h3>
                <div class="am-list-item-text poem_list_one_text">作者：${bean.author}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;朝代：${bean.dynasty}</div>
            </div>
            <div class="am-u-sm-4 am-list-thumb">
              <img src="${basePath}${bean.image}" class="poem_list_one_img" />
            </div>
          </a>
        </li>
        </c:forEach>
        <!--for循环-->
    </ul>
    </div>
    </div>
  </div>
</div>
  <!--      主要内容        -->
  <!--      js        -->
<script src="js/jquery.min.js"></script>
<script src="js/amazeui.min.js"></script>
<script>
$(function(){

    // 动态计算古诗文列表文字样式
    auto_resize();
    $(window).resize(function() {
        auto_resize();
    });
    $('.am-list-thumb img').load(function(){
        auto_resize();
    });

    $('.am-list > li:last-child').css('border','none');
    function auto_resize(){
        $('.poem_list_one_nr').height($('.poem_list_one_img').height());

    }
        $('.poem_nav_gengduo').on('click',function(){
            $('.poem_more_list').addClass('poem_more_list_show');
       });
        $('.poem_more_close').on('click',function(){
            $('.poem_more_list').removeClass('poem_more_list_show');
        });
});
</script>
  <!--      //js        -->
</body>
</html>