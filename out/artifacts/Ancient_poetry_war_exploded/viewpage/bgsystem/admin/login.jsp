<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	request.setAttribute("basePath", basePath);
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>登录界面</title>
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,user-scalable=no">
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/login.css" />
</head>

<body style="background: url(images/bg.jpg) no-repeat;">
	<div class="container wrap1" style="height: 450px;">
		<h2 class="mg-b20 text-center">古诗文信息管理系统</h2>
		<div
			class="col-sm-8 col-md-5 center-auto pd-sm-50 pd-xs-20 main_content">
			<p class="text-center font16">用户登录</p>
			<form action="toLogin.do" method="post">
				<div class="form-group mg-t20">
					<span class="glyphicon glyphicon-user"></span> <input type="text"
						class="login_input" placeholder="请输入用户名" name="managerName" />
				</div>
				<div class="form-group mg-t20">
					<span class="glyphicon glyphicon-lock"></span> <input
						type="password" class="login_input" placeholder="请输入密码"
						name="password" />
				</div>
				<div class="checkbox mg-b25">
					<label /><label /><label /><label /><label /><label
						style="color: red;">${error}</label>
				</div>
				<button type="submit" class="login_btn">登 录</button>
			</form>
		</div>
		<!--row end-->
	</div>
	<!--container end-->
</body>
</html>