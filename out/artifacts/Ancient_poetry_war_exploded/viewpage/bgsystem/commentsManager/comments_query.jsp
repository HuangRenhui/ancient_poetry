<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
<head>
	<title>留言信息管理页</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />	
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<!-- font-awesome icons CSS -->
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<!-- //font-awesome icons CSS -->
	<!-- side nav css file -->
	<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
	<!-- side nav css file -->
	<!-- js-->
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/modernizr.custom.js"></script>
	<!-- //js-->
	<!--webfonts-->
	<link href="http://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
	<!--//webfonts--> 
	<!-- Metis Menu -->
	<script src="js/metisMenu.min.js"></script>
	<script src="js/custom.js"></script>
	<link href="css/custom.css" rel="stylesheet">
	<!--//Metis Menu -->
	<!--   base css   -->
	<link rel="stylesheet" href="css/base.css" />
	<!--   //base css   -->
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
	
	<!-- 页头  -->
	<jsp:include page="/viewpage/bgsystem/base/header.jsp"></jsp:include>
	<!-- //页头  -->

	<!--左固定导航-->
	<jsp:include page="/viewpage/bgsystem/base/sidebar.jsp"></jsp:include>
	<!--//左固定导航-->
    
	<!-- 主要内容开始-->
	<div id="page-wrapper">
		<!-- 留言信息内容展示开始-->
		<div class="appear">
			<h2 class="title1">留言信息管理</h2>
			<div class="blank-page widget-shadow scroll" id="style-2" style="position: relative;">
				<!--  form表单开始   -->
				<form class="input" action="${pageContext.request.contextPath}/manage_toCommentQuery.do" method="post">
					<!--选择框-->
					<div class="infobar">
						<select class="info" name="info">
							<option value="commentsId">留言号</option>
							<option value="wechatname">用户名</option>
							<option value="title">古诗文题目</option>
						</select>
					</div>
					<!--选择框-->
					<!--搜索框-->
					<div class="search-box" style="position: absolute;right:10%;top:5px;width:150px;">
						<input class="sb-search-input input__field--madoka" placeholder="搜索" type="text" id="input-31" name="search"/>
					</div><!--//搜索框-->
					<div class="search-bt">
						<!--   搜索按钮       -->
						<button type="submit" class="search"><i class="fa fa-search" aria-hidden="true"></i>搜索</button>
						<!--   //搜索按钮       -->
					</div>
				</form>
				<!--  form表单开始   -->
			</div>
			<div class="bs-example widget-shadow" data-example-id="bordered-table"> 		
				<table class="table table-bordered"> 
					<thead> 
						<tr> 
							<th>留言号</th> 
							<th>用户名</th> 
							<th>古诗文题目</th> 
							<th>详情</th>
						</tr> 
					</thead> 
					<tbody> 
						<c:if test="${listBean == null || listBean.size() == 0 }">
							<h1 align="center">查询结果为空</h1>
						</c:if>
						<c:if test="${listBean != null || listBean.size() != 0 }">
						<!-- for循环 开始 -->
						<c:forEach items="${listBean}" var="bean">
						<tr> 
							<td>${bean.commentsid }</td> 
							<td>${bean.userBean.getWechatname() }</td> 
							<td>${bean.poetryBean.getTitle() }</td> 
							<!--   详情按钮       -->
							<td><a href="${pageContext.request.contextPath}/manage_toCommentShow.do?id=${bean.commentsid }"><button type="button" class="edit" ><i class="fa fa-comment" aria-hidden="true"></i>详情</button></a></td>
							<!--   //详情按钮       -->
						</tr>
						</c:forEach> 
						<!-- for循环结束  -->
						</c:if>
					</tbody> 
				</table>
			</div>
			<!--  分页开始     -->
			<div class="blank-page widget-shadow scroll" id="style-2 div1" style="position: relative;">
				<div class="pagination">
					<c:if test="${page.isFirstPage==true}">
						<a class="common pa1 disabled">首页</a>
						<a class="common pa2 disabled">上一页</a>
					</c:if>
					<c:if test="${page.isFirstPage!=true}">
						<a class="common pa1" href="manage_toCommentQuery.do?pageNo=${page.firstPage}">首页</a>
						<a class="common pa2" href="manage_toCommentQuery.do?pageNo=${page.prePage}">上一页</a>
					</c:if>
					<c:if test="${page.pageNum == 0}"><a class="common pa3">第1页</a></c:if>
					<c:if test="${page.pageNum != 0}"><a class="common pa3">第${page.pageNum}页</a></c:if>
					<!-- 需要从数据库中获取数据  -->
					<c:if test="${page.isLastPage==true}">
						<a class="common pa2 disabled">下一页</a>
						<a class="common pa1 disabled">尾页</a>
					</c:if>
					<c:if test="${page.isLastPage!=true}">
						<a class="common pa2" href="manage_toCommentQuery.do?pageNo=${page.nextPage}">下一页</a>
						<a class="common pa1" href="manage_toCommentQuery.do?pageNo=${page.lastPage}">尾页</a>
					</c:if>
					<c:if test="${page.pages == 0}"><span>共1页</span></c:if>
					<c:if test="${page.pages != 0}"><span>共${page.pages}页</span></c:if>
				</div>
			</div>
			<!--  分页结束     -->
		</div>
		<!-- 留言信息内容展示结束-->
	</div>
	<!-- 主要内容结束-->
	<!--页脚-->
	<jsp:include page="/viewpage/bgsystem/base/footer.jsp"></jsp:include>
	<!--//页脚-->
</div>
	
	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
	  $('.sidebar-menu').SidebarNav()
	</script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
	<script src="js/classie.js"></script>
	<script>
		var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
			showLeftPush = document.getElementById( 'showLeftPush' ),
			body = document.body;
			
		showLeftPush.onclick = function() {
			classie.toggle( this, 'active' );
			classie.toggle( body, 'cbp-spmenu-push-toright' );
			classie.toggle( menuLeft, 'cbp-spmenu-open' );
			disableOther( 'showLeftPush' );
		};
		
		function disableOther( button ) {
			if( button !== 'showLeftPush' ) {
				classie.toggle( showLeftPush, 'disabled' );
			}
		}
	</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>
	<!-- Bootstrap Core JavaScript -->
   
</body>
</html>