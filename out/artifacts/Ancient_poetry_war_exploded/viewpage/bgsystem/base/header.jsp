<%@ page language="java" import="java.util.*,com.dao.bean.ManagerBean" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
	<!-- 页头开始   -->
	<div class="sticky-header header-section ">
		<div class="header-left" style="height:60px;">
			<!--切换按钮启动-->
			<button id="showLeftPush"><i class="fa fa-bars"></i></button>
			<!--切换按钮结束-->
			<a href="#" target="_top"><img src="images/logo.png" width="100px" style="float: left;" /></a>
			<a href="#" target="_top"><h1 style="margin-top:15px;font-family: '华文行楷';">古诗文信息管理系统</h1></a>
			<div class="clearfix"></div>
		</div>
		<div class="header-right">
			<!--  当前系统管理员信息    -->
			<div class="profile_details">		
				<ul>
					<li class="dropdown profile_details_drop">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<div class="profile_img">	
								<span class="prfil-img"><img src="images/2.jpg" alt=""> </span> 
								<div class="user-name">
									<p>${admin.name }</p>
									<span style="margin-left: 7px;">管理员</span>
								</div>
								<i class="fa fa-angle-down lnr"></i>
								<i class="fa fa-angle-up lnr"></i>
								<div class="clearfix"></div>	
							</div>	
						</a>
						<ul class="dropdown-menu drp-mnu">
							<li> <a href="${pageContext.request.contextPath}/manage_toSetting.do?id=${admin.managerid }"><i class="fa fa-cog"></i>设置</a> </li> 
							<li> <a href="manage_toAccount.do?id=${admin.managerid}"><i class="fa fa-user"></i>账户</a> </li> 
							<li> <a href="manage_toLogout.do"><i class="fa fa-sign-out"></i>退出系统</a> </li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="clearfix"> </div>				
		</div>
		<div class="clearfix"> </div>	
	</div>
	<!-- //页头开始  -->