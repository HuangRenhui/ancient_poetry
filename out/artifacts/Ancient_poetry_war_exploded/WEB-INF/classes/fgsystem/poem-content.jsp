<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <link rel="stylesheet" href="css/amazeui.min.css">
  <link rel="stylesheet" href="css/wap.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <title>古诗文内容页</title>
   <script type="text/javascript">
  function toAddPraise() {
	  var $praise = $('#praise').val()
	  var $commentsid = $('#commentsid').val()
/* 	   alert($praise); 
	  alert($commentsid);  */
	  $.ajax({
           url:'user_toAddPraise.do',
           data:{
        	   praise:$praise,
        	   id:$commentsid
           },
           type:'POST'/* ,
           error: function(request) {  
               alert("提交失败");  
           },
           success: function(data) {
        	   alert("提交成功"); 
           } */
      });
  }

  </script>
</head>
<body style="background:#ececec">
<div class="poem_mian" >
    <!--   头部   -->
    <div class="poem_head">
      <header data-am-widget="header"
              class="am-header am-header-default poem_head_block">
          <div class="am-header-left am-header-nav ">
              <!-- 返回超链接   -->
              <a href="#left-link" class="iconfont poem_head_jt_ico">&#xe601;</a>
              <!-- 返回超链接   -->
          </div>

          <h1 class="am-header-title poem_article_user">
            <span class="poem_name">每天一篇古诗文</span>
          </h1>
      </header>
    </div>
  <!--   头部   -->

  <!--   主体内容   -->
  <div class="poem_content">
  <div class="poem_content_block">
    <article data-am-widget="paragraph" class="am-paragraph am-paragraph-default poem_content_article" data-am-paragraph="{ tableScrollable: true, pureview: true }">
    <!--      诗题        -->
    <h1 class="poem_article_title">${bean.title }</h1>
    <!--      //诗题        -->

    <!--      作者       -->
    <div class="poem_article_user_author">作者：${bean.author }</div>
    <!--      //作者       -->

    <!--      朝代       -->
    <div class="poem_article_user_dynasty">朝代：${bean.dynasty }</div>
    <!--      //朝代       -->

    <!--      发布日期        -->
    <div class="poem_article_user_time">发布日期：${bean.date }</div>
    <!--      //发布日期        -->
    
    <!--      诗文内容图片        -->
    <img src="${basePath}${bean.image}">
    <!--      //诗文内容图片        -->

    <!--      诗文内容        -->
    <p style="text-align: center;">
      ${bean.content }
    </p>
    <!--      //诗文内容        -->

    <p><blockquote><p>背景</p></blockquote></p>
    <!--      诗文创作背景        -->
    <p>${bean.background }
    </p>
    <!--      诗文创作背景        -->

    <p><blockquote><p>注释</p></blockquote></p>
    <!--      诗文注释        -->
    <p>${bean.annotation }<br>
    </p>
    <!--      //诗文注释        -->

    <p><blockquote><p>译文</p></blockquote></p>
    <!--      诗文译文        -->
    <p>
      ${bean.translation }
    </p>
    <!--      //诗文译文        -->

    <p><blockquote><p>赏析</p></blockquote></p>
    <!--      诗文赏析        -->
    <p>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${bean.appreciation }
    <br>
    </p>
    <!--      //诗文赏析        -->
    </article>
    <!--      浏览与分享按钮     -->
    <ul class="like_share_block">
      <li><span class="link_browse_button"><i class="fa fa-eye"></i>146</span></li>
      <li><a class="link_share_button" href="###"><i class="iconfont share_ico_wx">&#xe630;</i>微信</a></li>
      <li><a class="link_share_button" href="###"><i class="iconfont share_ico_pyq">&#xe62e;</i>朋友圈</a></li>
    </ul>
    <!--      //浏览与分享按钮     -->

    <!--      习题与留言       -->
    <div class="poem_article_praxis-message">
      <div class="poem_article_praxis_title">
        <a href="${pageContext.request.contextPath}/user_toExerciseContentListByPoetryid.do?id=${bean.poetryid}">
          ${bean.title }习题练习
        </a>
      </div>
      <div class="poem_article_message_title">
        <a href="review-content.html">
          写留言
        </a>
      </div>
    </div>
    <!--      //习题与留言       -->
  </div>

  <!--      留言内容展示列表       -->
  <c:forEach items="${commentsList }" var="commentBean">
  <div class="poem_comment_list">
    <div class="poem_comment_list_wap"><div class="poem_comment_list_title">留言</div>

    <div data-am-widget="tabs" class="am-tabs am-tabs-default poem_comment_list_tab" >
        
        <div class="am-tabs-bd poem_pl_list">
            <div data-tab-panel-0 class="am-tab-panel am-active">
              <div class="poem_comment_list_block">
                <!--      微信用户头像       -->
                <div class="poem_comment_list_block_l">
                  <img src="images/a1.png" >
                </div>
                <!--      //微信用户头像       -->
                <div class="poem_comment_list_block_r">
                  <!--      微信用户名       -->
                  <div class="poem_comment_list_block_r_info">${commentBean.userBean.getWechatname() }</div>
                  <!--      //微信用户名       -->
                  <!--       微信用户留言内容       -->
                  <div class="poem_comment_list_block_r_text">${commentBean.content}</div>
                  <!--       微信用户留言内容       -->
                  <div class="poem_comment_list_block_r_bottom">
                    <!--     微信用户留言时间    -->
                    <div class="poem_comment_list_bottom_info_l">
                    <fmt:formatDate value="${commentBean.commenttime}" pattern="yyyy年MM月dd日 HH:mm:ss"/>              
                    </div>
                    <!--     //微信用户留言时间    -->
                    
                    <div class="poem_comment_list_bottom_info_r">
                      <!--     微信用户点赞数    -->
                      <input type="hidden" id="commentsid" value="${commentBean.commentsid}">
                      <input type="hidden" id="praise" value="${commentBean.praise}">
                      <span><i id="fa" class="fa fa-thumbs-o-up" aria-hidden="true" onclick="toAddPraise()"></i><span id="praise-txt" name="praise">${commentBean.praise}</span> </span>
                      
                      <!--     微信用户点赞数    -->
                      </div>
                      </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
  </c:forEach>
  <!--      //留言内容展示列表       -->

  <!--      页脚       -->
  <div class="poem_article_footer_info">Copyright(c)2018 软工81班.阿尔法小队</div>
  <!--      页脚       -->

  <!--      js       -->
  <script src="js/jquery.min.js"></script>
  <script src="js/amazeui.min.js"></script>
  <script>
  $(function(){

      // 动态计算古诗文列表文字样式
      auto_resize();
      $(window).resize(function() {
          auto_resize();
      });
      $('.am-list-thumb img').load(function(){
          auto_resize();
      });
      $('.poem_article_like li:last-child').css('border','none');
          function auto_resize(){
          $('.poem_list_one_nr').height($('.poem_list_one_img').height());
                  // alert($('.poem_list_one_nr').height());
      }
          $('.poem_article_user').on('click',function(){
              if($('.poem_article_user_info_tab').hasClass('poem_article_user_info_tab_show')){
                  $('.poem_article_user_info_tab').removeClass('poem_article_user_info_tab_show').addClass('poem_article_user_info_tab_cloes');
              }else{
                  $('.poem_article_user_info_tab').removeClass('poem_article_user_info_tab_cloes').addClass('poem_article_user_info_tab_show');
              }
          });

          $('.poem_head_gd_ico').on('click',function(){
              $('.poem_more_list').addClass('poem_more_list_show');
         });
          $('.poem_more_close').on('click',function(){
              $('.poem_more_list').removeClass('poem_more_list_show');
          });

          /*     点击点赞字体图标自动加一     */
          $('#fa').click(function(){
              var num = $('#praise-txt').text();
              num++;
              $('#praise-txt').text(num);
              $('.fa-thumbs-o-up').removeClass('fa-thumbs-o-up').addClass('fa fa-thumbs-up');
          });
  });
  </script>
  <!--      //js       -->
</body>
</html>