<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <link rel="stylesheet" href="css/amazeui.min.css">
  <link rel="stylesheet" href="css/wap.css">
  <title>古诗文搜索内容列表页</title>
</head>
<body style="background:#ececec">
  <!--     主体内容     -->
  <div class="poem_mian" >
    <!--    头部      -->
    <div class="poem_head">
      <header data-am-widget="header"
          class="am-header am-header-default poem_head_block">

        <div class="am-header-left am-header-nav ">
          <!--    返回超链接      -->
          <a href="${pageContext.request.contextPath}/user_toIndex.do" class="iconfont poem_head_jt_ico">&#xe601;</a>
          <!--    //返回超链接      -->
        </div>
         <h1 class="am-header-title poem_article_user">
            <span class="poem_name">每天一篇古诗文</span>
          </h1>
      </header>
    </div>
    <!--    头部      -->

    <!--    古诗文搜索列表    -->
    <div class="poem_content poem_content_list">
      <div class="poem_article_like">
        <div class="poem_content_main poem_article_like_delete">
          <div data-am-widget="list_poems" class="am-list-poems am-list-poems-default am-no-layout">
            <div class="am-list-poems-bd">
              <ul class="am-list">
              	<c:if test="${listBean == null || listBean.size() == 0 }">
					<h1 align="center">查询结果为空</h1>
				</c:if>
				<c:if test="${listBean != null || listBean.size() != 0 }">
				<!-- for循环 开始 -->
				<c:forEach items="${listBean}" var="bean">
                <li class="am-g am-list-item-desced am-list-item-thumbed am-list-item-thumb-right poem_list_one_block">
                  <a href="${pageContext.request.contextPath}/user_toPoemContentByPoetryid.do?id=${bean.poetryid}" class="">
                    <div class=" am-u-sm-8 am-list-main poem_list_one_nr">
                        <h3 class="am-list-item-hd poem_list_one_bt">${bean.title}</h3>
                        <div class="am-list-item-text poem_list_one_text">作者：${bean.author}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;朝代：${bean.dynasty}</div>
                    </div>
                      <div class="am-u-sm-4 am-list-thumb">
                        <img src="${basePath}${bean.image}" class="poem_list_one_img" />
                      </div>
                    </a>
                </li>
                <!--for循环-->
                </c:forEach>
                </c:if>
              </ul>
            </div>
          </div>
        </div>
    </div>
    <!--    古诗文搜索列表    -->
    <!--      js        -->
    <script src="js/jquery.min.js"></script>
    <script src="js/amazeui.min.js"></script>
    <script>
      $(function(){

          // 动态计算古诗文列表文字样式
          auto_resize();
          $(window).resize(function() {
              auto_resize();
          });
          $('.am-list-thumb img').load(function(){
              auto_resize();
          });
          $('.poem_article_like li:last-child').css('border','none');
              function auto_resize(){
              $('.poem_list_one_nr').height($('.poem_list_one_img').height());
                      // alert($('.poem_list_one_nr').height());
          }
              $('.poem_article_user').on('click',function(){
                  if($('.poem_article_user_info_tab').hasClass('poem_article_user_info_tab_show')){
                      $('.poem_article_user_info_tab').removeClass('poem_article_user_info_tab_show').addClass('poem_article_user_info_tab_cloes');
                  }else{
                      $('.poem_article_user_info_tab').removeClass('poem_article_user_info_tab_cloes').addClass('poem_article_user_info_tab_show');
                  }
              });
              $('.poem_head_gd_ico').on('click',function(){
                  $('.poem_more_list').addClass('poem_more_list_show');
             });
              $('.poem_more_close').on('click',function(){
                  $('.poem_more_list').removeClass('poem_more_list_show');
              });
      });
  </script>
  <!--      js        -->
</body>
</html>