<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	request.setAttribute("basePath", basePath);
%>

<!DOCTYPE HTML>
<html>
<head>
<base href="<%=basePath%>">
<title>后台管理首页</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet">
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet'
	type='text/css' />
<!-- //side nav css file -->

<!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link
	href="http://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext"
	rel="stylesheet">
<!--//webfonts-->

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->

<!-- requried-jsfiles-for owl -->
<link href="css/owl.carousel.css" rel="stylesheet">
<script src="js/owl.carousel.js"></script>
<script>
	$(document).ready(function() {
		$("#owl-demo").owlCarousel({
			items : 3,
			lazyLoad : true,
			autoPlay : true,
			pagination : true,
			nav : true,
		});
	});
</script>
<!-- //requried-jsfiles-for owl -->
</head>
<body class="cbp-spmenu-push">
	<div class="main-content">

		<!-- 页头  -->
		<jsp:include page="/viewpage/bgsystem/base/header.jsp"></jsp:include>
		<!-- //页头  -->

		<!--左固定导航-->
		<jsp:include page="/viewpage/bgsystem/base/sidebar.jsp"></jsp:include>
		<!--//左固定导航-->

		<!-- 主要内容开始-->
		<div id="page-wrapper">
			<!-- for amcharts js -->
			<script src="js/amcharts.js"></script>
			<script src="js/serial.js"></script>
			<script src="js/export.min.js"></script>
			<link rel="stylesheet" href="css/export.css" type="text/css"
				media="all" />
			<script src="js/light.js"></script>
			<!-- for amcharts js -->

			<script src="js/index1.js"></script>
			<!-- 轮播开始   -->
			<div class="charts" style="margin-top: 100px;">
				<div class="mid-content-top charts-grids">
					<div class="middle-content">
						<h4 class="title"
							style="font-weight: bold; font-family: '微软雅黑'; text-align: center;">欢迎您！${admin.name }</h4>
						<!-- start content_slider -->
						<div id="owl-demo" class="owl-carousel text-center">
							<div class="item">
								<img class="lazyOwl img-responsive"
									data-src="images/slider1.jpg" alt="name">
							</div>
							<div class="item">
								<img class="lazyOwl img-responsive"
									data-src="images/slider2.jpg" alt="name">
							</div>
							<div class="item">
								<img class="lazyOwl img-responsive"
									data-src="images/slider3.jpg" alt="name">
							</div>
							<div class="item">
								<img class="lazyOwl img-responsive"
									data-src="images/slider4.jpg" alt="name">
							</div>
							<div class="item">
								<img class="lazyOwl img-responsive"
									data-src="images/slider5.jpg" alt="name">
							</div>
							<div class="item">
								<img class="lazyOwl img-responsive"
									data-src="images/slider6.jpg" alt="name">
							</div>
							<div class="item">
								<img class="lazyOwl img-responsive"
									data-src="images/slider7.jpg" alt="name">
							</div>
						</div>
					</div>
					<!--//轮播结束---->
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<!--页脚-->
		<jsp:include page="/viewpage/bgsystem/base/footer.jsp"></jsp:include>
		<!--//页脚-->
	</div>

	<!-- Classie -->
	<!-- for toggle left push menu script -->
	<script src="js/classie.js"></script>
	<script>
		var menuLeft = document.getElementById('cbp-spmenu-s1'), showLeftPush = document
				.getElementById('showLeftPush'), body = document.body;

		showLeftPush.onclick = function() {
			classie.toggle(this, 'active');
			classie.toggle(body, 'cbp-spmenu-push-toright');
			classie.toggle(menuLeft, 'cbp-spmenu-open');
			disableOther('showLeftPush');
		};

		function disableOther(button) {
			if (button !== 'showLeftPush') {
				classie.toggle(showLeftPush, 'disabled');
			}
		}
	</script>
	<!-- //Classie -->
	<!-- //for toggle left push menu script -->

	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->

	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
		$('.sidebar-menu').SidebarNav()
	</script>
	<!-- //side nav js -->

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js">
		
	</script>
	<!-- //Bootstrap Core JavaScript -->

</body>
</html>